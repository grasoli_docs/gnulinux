\chapter{Organización del sistema en los discos}
\label{discos}
% Dependencias: Primeros pasos con la línea de comandos

\begin{enumerate}
    \item Conceptos:
        \begin{itemize}
            \item ¿Qué significa montar? ¿Qué es un punto de montaje?
        \end{itemize}
    \item Principio de diseño: los dispositivos son archivos
    \item Área de intercambio (``swap'')
    \item Estándar de la jerarquía del sistema de archivos (FHS):
        \begin{enumerate}
            \item Principios:
                \begin{itemize}
                    \item compartido/no compartido
                    \item estático/dinámico
                \end{itemize}
            \item Directorios principales
        \end{enumerate}
    \item Archivo: ``/etc/fstab''
    \item Comandos:
        \begin{itemize}
            \item \texttt{dd}
            \item \texttt{findmnt}, \texttt{mount}, \texttt{umount}
            \item \texttt{cfdisk}, \texttt{parted} (mencionar
                  \texttt{fdisk} y \texttt{sfdisk} al pasar)
            \item \texttt{fsck}, \texttt{mkfs}
        \end{itemize}
\end{enumerate}

Este capítulo trata sobre cómo se organiza un sistema GNU/Linux en torno a
los discos donde se almacena la información, considerando distintos niveles
de abstracción.

\section{Conceptos básicos}

\subsection{Medios de almacenamiento}

Para empezar esta sección, tenemos que tener en claro qué entendemos por
\emph{dispositivo de almacenamiento} o \emph{medio de almacenamiento}.
Llamamos así a cualquier pedazo de hardware que nos permita almacenar datos
persistentemente y más tarde acceder a ellos: un \emph{disco duro}, un
\emph{disquete}, un \emph{CD}, un \emph{DVD}, un ``\emph{pendrive}''. Es
importante aclarar que, al menos para nosotros, el almacenamiento tiene que
ser persistente, es decir, después de que apaguemos la computadora o
desconectemos el dispositivo, los datos tienen que seguir ahí, guardados para
poder ser accedidos en el futuro. Así, por ejemplo, no vamos a considerar
dispositivo de almacenamiento a la memoria RAM, ya que todo lo que en ella
guardamos lo perdemos al reiniciar, ni mucho menos a los registros internos
de la CPU.

Nosotros estamos acostumbrados a manejar en nuestro sistema un montón de
\emph{archivos} y \emph{directorios}. Por medio de ellos organizamos nuestra
información personal y accedemos a recursos del sistema operativo.

Ahora, archivo y directorio son conceptos abstractos, de alto nivel. Lo que
al fin y al cabo se guarda en el disco duro (o el medio que sea) no son
archivos ni directorios, sino una secuencia (larguísima) de ceros y unos,
representados de alguna forma física. A nosotros no nos interesa la
tecnología física empleada para almacenar los datos: podemos quedarnos
tranquilos ya que el mismo hardware se encarga de hacerle saber al sistema
operativo dónde hay un cero y dónde hay un uno, y así este puede manejar los
datos como números binarios.

\subsection{Sistemas de archivos}

Todo muy lindo, pero los archivos y directorios están ahí y por más que
reiniciemos la máquina siguen estando ahí. Así que de alguna forma se
almacenan en los dispositivos de almacenamiento. Dado que lo único que hay en
estos son ceros y unos, lo que se hace es buscar una representación binaria
de aquellos.

Además, no son solo los contenidos de los archivos lo que se guarda, sino
también toda la jerarquía en torno a la cual se organizan. Esto incluye los
nombres de los archivos, varios atributos de los mismos (como la fecha de
modificación o los permisos) y, por supuesto, las relaciones de parentesco:
saber en qué directorio está cada archivo y en qué directorio está cada
directorio. Toda esta información se estructura de acuerdo a lo que se
denomina un \emph{sistema de archivos}, que podría pensarse como el formato
en que los archivos y directorios se guardan y asocian en un medio de
almacenamiento.

Existen diversos sistemas de archivos, cada uno estructura los datos a su
manera y tiene sus ventajas y desventajas. Difieren, por ejemplo, en el
máximo tamaño permitido para un archivo, en el grado de fragmentación, en
otros detalles de rendimiento o en cómo son capaces de responder ante
escándalos de corrupción. No cualquier sistema de archivos funciona en
cualquier plataforma, ya que el sistema operativo debe encargarse de
implementarlo. En el caso de GNU/Linux, los sistemas de archivos se
implementan mayormente en el núcleo, Linux. Algunos ejemplos son:
\begin{description}
    \item[ext2, ext3, ext4]
        El más popular en GNU/Linux desde hace tiempo, al que se le han ido
        haciendo mejoras progresivas. ext4 es relativamente joven: es estable
        desde la navidad de 2008.
    \item[ReiserFS]
        Famoso por su creador, Hans Reiser, quien mató a su mujer y la
        enterró en una colina a 8 cuadras de su casa; está preso desde 2008.
    \item[ZFS]
        Muy poderoso, desarrollado por Sun Microsystems. En Linux no está
        disponible por problemas de licencias y patentes.
    \item[BtrFS]
        Bastante nuevo y prometedor, todavía inestable. Comparable a ZFS pero
        hecho específicamente para el núcleo Linux.
    \item[FAT]
        Introducido por MS-DOS y empleado por los Windows de la rama DOS. Es
        muy viejo y limitado pero se lo aguanta cualquier sistema, incluido
        GNU/Linux. Por esta razón todavía se usa mucho en ``pendrives'', ya
        que así se garantiza el acceso desde cualquier lado.
    \item[NTFS]
        El usado por los Windows de la rama NT.
        \marginpar{Investigar sobre NTFS: ¿hubo que hacer ingeniería inversa
                   para que Linux pudiera manejarlo? Hasta hace algunos años
                   solo lo podía leer, hoy también lo puede escribir.}
\end{description}

En gran medida, al usuario de escritorio le da igual la elección del sistema
de archivos. ext4 es una buena opción si no sabés cuál seleccionar a la hora
de instalar GNU/Linux; y para cuando necesites acceso desde Windows,
probablemente tengas que decantarte por FAT o NTFS. Con estos sencillos
lineamientos no deberías tener problemas. Para usuarios exigentes o
servidores, en cambio, esta elección se torna más importante.

Cada medio de almacenamiento tiene asociado, en principio, un único sistema
de archivos. Es decir, podés tener un disco duro con ext4, otro con BtrFS y
un pendrive con FAT. El proceso de asignar un sistema de archivos determinado
a un medio, de modo que empiece a emplearlo a partir de entonces, se denomina
\emph{formatear}, en concordancia con el concepto que vimos de sistema de
archivos como el formato para guardar y asociar archivos y directorios. Al
formatear, se pierde el sistema de archivos que había antes, y con él, las
referencias a todos los datos guardados; si bien los contenidos pueden seguir
estando ahí, ya no tenemos forma de acceder a ellos salvo que hagamos magia
negra\footnote{Hay gente con alta iluminación arcana que sabe hacer magia
  negra:
  \textsf{http://www.grulic.org.ar/~mdione/glob/posts/recovering-partitions-with-pen-and-paper/}.
  Lamentablemente no encontramos el artículo original, que estaba en
  español.}, y en cualquier momento se pueden sobreescribir con nuevos datos.

\subsection{Particiones}

La restricción a un solo sistema de archivos por dispositivo a veces se torna
una gran limitación por cuestiones que dentro de poco vamos a ver.
Afortunadamente hay una solución: podés partir el dispositivo en varios
pedazos y usar cada uno como un medio de almacenamiento distinto. Y no hace
falta que vayas a buscar un hacha, ya la incorpora el sistema operativo,
porque tales divisiones son virtuales. Cada pedazo resultante del dispositivo
se llama, sugestivamente, \emph{partición}. Entonces, podés agarrar tu disco
duro con ext4 y hacerle 2 particiones; cuando las hayas hecho, el ext4 que
había antes se habrá perdido, y en su lugar vas a poder formatear cada
partición con el sistema de archivos que quieras, que puede ser el mismo para
ambas o no.

\section{FHS}

Ahora vamos a hablar del estándar \emph{FHS} (en inglés, ``Filesystem
Hierarchy Standard'', es decir, Estándar de la jerarquía del sistema de
archivos).

\begin{description}
    \item[/bin]
    \item[/boot]
    \item[/etc]
    \item[/home]
    \item[/lib]
    \item[/mnt]
    \item[/media]
    \item[/opt]
    \item[/proc]
    \item[/root]
    \item[/run]
    \item[/sbin]
    \item[/srv]
    \item[/sys]
    \item[/usr]
        \begin{description}
            \item[/usr/bin]
            \item[/usr/include]
            \item[/usr/lib]
            \item[/usr/local]
                \begin{description}
                    \item[/usr/local/bin]
                    \item[/usr/local/include]
                    \item[/usr/local/lib]
                    \item[/usr/local/sbin]
                    \item[/usr/local/share]
                \end{description}
            \item[/usr/sbin]
            \item[/usr/share]
            \item[/usr/src]
        \end{description}
    \item[/tmp]
    \item[/var]
        \begin{description}
            \item[/var/cache]
            \item[/var/lib]
            \item[/var/log]
            \item[/var/spool]
            \item[/var/tmp]
        \end{description}
\end{description}

\section{Más información}

\begin{itemize}
    \item Páginas de manual: \texttt{man 5 filesystems}, \texttt{man 5 hier}
    \item Página web sobre el FHS:
          \textsf{https://wiki.linuxfoundation.org/en/FHS}
\end{itemize}
