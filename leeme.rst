=========================================
  Guía de inmersión en GNU/Linux: leeme
=========================================


Esta **guía de inmersión en GNU/Linux** es un proyecto que estamos
llevando a cabo desde el GrASoLi, como complemento del taller de
GNU/Linux que venimos realizando. ¿Qué es el GrASoLi?: toda la
información al respecto la podés encontrar en el mismo libro, en
resumen diremos por acá que es un grupo de software libre de una
facultad argentina.


¿Cómo obtener un PDF con el material?
=====================================

El material está escrito en el lenguaje de marcado *LaTeX*. Para
generar un PDF, hace falta primero obtener los archivos fuente del
repositorio donde están subidos, y luego compilarlos.

Obtener las fuentes
-------------------

Las fuentes están disponibles en *gitorious*, un sitio web libre que
ofrece repositorios *git* de forma gratuita.

*git* es un programa que permite generar repositorios con todos los
archivos correspondientes a un proyecto, mantener un historial de
cambios de cada archivo, sincronizar repositorios entre sí tanto de
forma local como remota y mucho más. Es, en síntesis, un sistema de
control de revisiones descentralizado.

Para descargar nuestro repositorio basta con instalar git (en
GNU/Linux generalmente existe un paquete ``git``) y ejecutar el
siguiente comando::

    $ git clone git://gitorious.org/grasoli_docs/gnulinux.git

Esto creará un directorio ``gnulinux`` dentro del cual estarán todos
los archivos del repositorio.

Compilar las fuentes
--------------------

Primero entramos al directorio creado en el paso anterior::

    $ cd gnulinux

Una manera de conseguir un PDF es compilar las fuentes en LaTeX a DVI
y después pasar a PDF::

    $ latex gnulinux.tex
    $ dvipdf gnulinux.dvi

Esto va a generar el archivo ``gnulinux.pdf``. Para usar estos
comandos necesitás tener instalados algunos paquetes; en la
distribución Arch Linux, estos son ``texlive-core`` y ``ghostscript``.

Otra manera es compilar directamente a PDF::

    $ pdflatex gnulinux.tex

Para esto necesitás, en Arch, el paquete ``texlive-bin``.

En otras distribuciones de GNU/Linux los nombres de los paquetes
pueden variar.
